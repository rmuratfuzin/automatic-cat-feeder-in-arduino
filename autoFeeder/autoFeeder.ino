/*
Используемые пины:
A2 - для перевода в сервисный режим (тумблер)
A3 - кнопка для уменьшения времени
А4 - переключение между настройкой длительности и периодичности открытия. длительность в секундах, продолжительность в часах.
А5 - увеличение времени.
все кнопки подтянуты через INPUT_PULLUP, т.е. их необходимо соединять с землёй, использовать нормально разомкнутые кнопки.

Дисплей:
подклбючается на 4 и 5 цифровые выходы. (распиновку см. ниже по коду)

Серво - 10й цифровой пин.
*/

//Библиотека для работы с серво 
#include <Servo.h> 
Servo myservo;

//Библиотека для работы с дисплеем
#include "TM1637.h" 

//работа с постоянной памятью
#include <EEPROM.h>  



//КОНСТАНТЫ И НАСТРОЙКИ:
//пины в которые будут подключены соответствующие выводы дисплея TM1637
#define CLK 5
#define DIO 4

/*пины в которые будут подключены кнопки: 
service - тумблер для перевода в режим настройки
minus - кнопка для уменьшения времени при настройке
ok - переключение между настройкой продолжительности (в секундах) и периода (в часах) открытия сервы
plus - кнопка для увеличения времени при настройке
*/
#define service A2
#define minus A3 
#define ok A4
#define plus A5

//флаг для переключения между режимами настройки частоты и продолжительности открытия сервы.
boolean seeHors = true;

//дефолтные параметры продолжительности (в секундах) и периода (в часах) открытия сервы
byte seconds;
byte hours;

//адреса в который будет сохраняться заданное количество секунд (часы будут +1)
int adress = 0;

//порт в который будет подключена серво:
#define servo 10

/*
текст который будет отображаться в момент открытия сервы.
можно попробовать расирить список возможных символов
http://arduino.on.kg/obzor-displeya-na-baze-drayvera-TM1637
{15, 14, 14, 13} = FEEd
*/
int8_t text [] = {15, 14, 14, 13};

//Создаём объект класса TM1637, в качестве параметров
//передаём номера пинов подключения
TM1637 tm1637(CLK, DIO);


//переменная для таймера
uint32_t fireTime;


void setup() {

//по умолчанию, в ячейках записано 255, рекомендую сперва отдельным скетчем записать первые значения в память
seconds = EEPROM.read(adress);
hours = EEPROM.read(adress+1);

//записываем время старта
fireTime = millis();

//Инициализация дисплея
  tm1637.init();
//Установка яркости горения сегментов
  /*
     BRIGHT_TYPICAL = 2 Средний
     BRIGHT_DARKEST = 0 Тёмный
     BRIGHTEST = 7      Яркий
  */
  tm1637.set(BRIGHT_TYPICAL);

//открываем порты для кнопок
pinMode (minus, INPUT_PULLUP);
pinMode (ok, INPUT_PULLUP);
pinMode (plus, INPUT_PULLUP);
pinMode (service, INPUT_PULLUP);


//вывод в консаль для отладки
//Serial.begin(9600);


//  инициаализация серво и установка её в ноль c последующим отключением
myservo.attach(servo);
myservo.write(0);
delay (1000);
myservo.detach();

}

void loop() {

//считываем данные по кнопка -, ОК, +, service
 boolean buttonService = !digitalRead (service);
 boolean buttonMin = !digitalRead(minus);
 boolean buttonOK = !digitalRead (ok);
 boolean buttonPlus = !digitalRead (plus);

switch((int)buttonService)
{
  // если включен сервисный режим, то появляется возможность настройки длительности и периода срабатывания
  case 1:

  //переключение между настройкой периодичности и продолжительности
  if (buttonOK == true){
      if (seeHors == true){
        EEPROM.update((adress + 1),hours);}
      else{
        EEPROM.update(adress,seconds);}
  seeHors = !seeHors;
  }
  
  // настройка секунд открытия сервы
  if ( seeHors == false ) {
     showTme (seconds, 5);

    if (buttonMin == true){
      seconds = dissCountTime (seconds) ;
      showTme (seconds, 5);}

    if (buttonPlus == true){
      seconds = countTime (seconds) ;
      showTme (seconds, 5);}
    }
  //настройка периодичности открытия сервы
  else {
     showTme (hours, 1);

    if (buttonMin == true){
      hours = dissCountTime (hours) ;
      showTme (hours, 1); }

    if (buttonPlus == true){
      hours = countTime (hours) ;
      showTme (hours, 1);  }
  }
    break;

//если сервисный режим выключен, то по ранее настроенной периодичности открываем серву на заданное количество секунд
    case 0:
    tm1637.clearDisplay();

  // отображаем количество минут до следующего открытия
   tm1637.display(((int)(((hours * 3600000) - (millis() - fireTime))/60000)) + 1 );

  //проверка прошло ли заданное количество часов
  if (millis() - fireTime >= (hours * 3600000)){ //в часе 3600000 в минуте 60000
    tm1637.clearDisplay();
    tm1637.display(text);
    
    //  инициаализация серво и поворот на 90 градусов, затем delay на настроенное количество секунд
    myservo.attach(servo);
    myservo.write(90);
    delay (seconds * 1000);
  
  // поворачиваем серву обратно в ноль и отключаем
    myservo.write(0);
    delay (1000);
    myservo.detach();
   
  //запоминаем время последнего срабатывания
    fireTime = millis();
    }
    break;
}
delay (1000);
}

byte dissCountTime (byte time){
  if (time > 0) {
    time --;
    return (time);  
  }
  else {
     return (time);
    }
}

byte countTime (byte time){
  if (time < 9) {
    time ++;
    return (time);  
  }
  else {
      return (time);
    }
}

void showTme (byte time, byte typeTime){
    tm1637.clearDisplay();
    tm1637.display(0, typeTime);
    tm1637.display(3, time);
}
